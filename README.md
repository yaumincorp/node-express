# Getting Started with Express (Node JS)

The project was created with express : https://expressjs.com/

# Front-end

Link to the front-end : https://gitlab.com/yaumincorp/typescript

## Available Script

In the project directory, you can run:

### `nodemon`

Runs the app in the development mode.\
Open [http://localhost:3001](http://localhost:3001) to view it in the browser.
