//Connexion to mongo db
const MongoClient = require("mongodb").MongoClient;
require("dotenv").config();

let dbConnection;

const uri = process.env.MONGODB_URI;
const dbName = "hangman-db";

MongoClient.connect(uri, (err, client) => {
  if (err) throw err;
  dbConnection = client.db(dbName);
  console.log("Connexion to database OK");
});

module.exports = {
  getDb: function () {
    return dbConnection;
  },
};
