var express = require("express");
const { getDb } = require("../connexion");
const ObjectId = require("mongodb").ObjectId;
var router = express.Router();
const bcrypt = require("bcrypt");
const { v4: uuidv4 } = require("uuid");

/* GET users listing. */

//let userScore = {};
let users = [];
let usersId = [];

const getUniqueListBy = (arr, key) => {
  return [...new Map(arr.map((item) => [item[key], item])).values()];
};

let total = 0;
router.get("/", function (req, res, next) {
  const dbConnect = getDb();

  const skip = req.query.skip;
  const limit = req.query.limit;

  dbConnect
    .collection("user")
    .aggregate([
      {
        $lookup: {
          from: "hangman",
          localField: "_id",
          foreignField: "userId",
          as: "score",
        },
      },
      {
        $facet: {
          users: [
            { $match: {} },
            { $sort: { _id: -1 } }, // Use this to sort documents by newest first
            { $skip: Number(skip) }, // Always apply 'skip' before 'limit'
            { $limit: Number(limit) }, // This is your 'page size'
          ],
          total: [{ $count: "count" }],
        },
      },
      {
        $set: {
          total: { $first: "$total.count" },
        },
      },
    ])
    .toArray((err, result) => {
      if (err) throw err;
      const isMore = result[0].total > Number(skip) + Number(limit);
      res.send({ users: result[0].users, total: result[0].total, isMore });
    });
});

/* GET user */
let score;
router.get("/user/:id", function (req, res, next) {
  const dbConnect = getDb();
  dbConnect
    .collection("hangman")
    .find({ userId: ObjectId(req.params.id) })
    .toArray((err, result) => {
      if (err) throw err;
      score = result[0];
    });
  dbConnect
    .collection("user")
    .find(ObjectId(req.params.id))
    .toArray((err, result) => {
      if (err) throw err;

      if (score) {
        res.send({
          ...result[0],
          scoreId: score._id,
          difficulty: score.difficulty,
          score: score.score,
        });
      } else {
        res.send({
          ...result[0],
        });
      }
    });
});

router.get("/check", (req, res, next) => {
  const dbConnect = getDb();
  //const checkPassword = await bcrypt.compare(userData.password, userFilter.password);
  //if (!checkPassword) return res.status(401).json({ error: 'Adresse email ou mot de passe incorrect' }).end();

  const textPassword = req.query.password;

  let password;

  dbConnect
    .collection("user")
    .find({ username: req.query.username })
    .toArray((err, user) => {
      if (err) throw err;

      password = user[0].password;
      const result = bcrypt.compareSync(textPassword, password);

      //console.log("RESULT PASSWORD", result); // true or false

      res.status(200).json({ result, user: user[0] });
    });
});

router.post("/login", function (req, res, next) {
  res.send({
    token: uuidv4(),
  });
});

router.post("/user/create", function (req, res, next) {
  const dbConnect = getDb();

  const saltRounds = 10;
  const salt = bcrypt.genSaltSync(saltRounds);

  const cryptedPassword = bcrypt.hashSync(req.body.password, salt);

  const dataToSend = { ...req.body, password: cryptedPassword };

  dbConnect.collection("user").insertOne(dataToSend);

  res.status(201).json({ message: "User created !" });
});

router.post("/hangman/createScore", function (req, res, next) {
  const dbConnect = getDb();

  const dataToSend = {
    userId: ObjectId(req.body.userId),
    difficulty: req.body.difficulty,
    score: req.body.score,
  };

  dbConnect.collection("hangman").insertOne(dataToSend);

  res.status(201).json({ message: "Score stored !" });
});

router.put("/hangman/updateScore", function (req, res, next) {
  const dbConnect = getDb();
  console.log("COUCOU JE PUT", req.body);
  dbConnect
    .collection("hangman")
    .updateOne({ _id: ObjectId(req.body.scoreId) }, [
      { $set: { score: req.body.score } },
    ]);

  res.status(201).json({ message: "Score updated !" });
});

module.exports = router;
