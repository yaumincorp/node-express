var express = require("express");
const fetch = require("node-fetch");
var router = express.Router();

router.get("/", async (req, res, next) => {
  const apiResponse = await fetch(
    "https://frenchwordsapi.herokuapp.com/api/word?nbrWordsNeeded=20"
  );
  console.log("API RESPONSE", apiResponse);
  const apiResponseJson = await apiResponse.json();

  const randomArray = apiResponseJson.map((item) =>
    item.WordName.normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      .toUpperCase()
  );

  res.status(200).send(randomArray);
});

module.exports = router;
